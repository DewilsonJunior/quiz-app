<?php

    include_once("include/SqlScripts.php");

    $action = $_POST["action"];

    switch($action){
        case "home":
            
            $cols = ["id","nome"];
            $condition = "where deleted = false
                            order by created desc";

            $result = SqlScripts::select($cols, "quiz", $condition);

            echo json_encode($result);
        break;

        case "carrega_quiz":

            $quiz_id = $_POST["quiz_id"];

            $result = array();
            $resp_ids = array();
            
            $perg_cols = ['id','titulo'];
            $perg_condition =   "where quiz_id = ". $quiz_id .
                                    " and deleted = false";

            $perguntas = SqlScripts::select($perg_cols, "perguntas", $perg_condition);

            foreach($perguntas as $pergunta){
                $resp_ids[] = $pergunta['id'];
            }

            $resp_cols = ['id','resposta','pergunta_id'];
            $resp_condition =   "where pergunta_id in ('". implode("','",$resp_ids) . "')" . 
                                    "and deleted = false";

            $respostas = SqlScripts::select($resp_cols, "respostas", $resp_condition);

            foreach($perguntas as $index => $pergunta){
                $result[]['pergunta'] = $pergunta;

                foreach($respostas as $resposta){
                    if($resposta['pergunta_id'] == $pergunta['id']){
                        
                        $result[$index]['pergunta']['resposta'][] = $resposta;
                    }
                }
            }

            echo json_encode($result);

        break;

        case "persistir":
            
            $data = $_POST["dados"];
            $resp_ids = [];
            $score = [];
            $pontuacao = 0;

            $score["duracao"] = $data['time'];
            unset($data['time']);

            foreach($data as $value){
                $resp_ids[] = $value;
            }

            $cols = ['id','correta'];
            $condition =    "where id in ('" . implode("','", $resp_ids) . "')
                                and deleted = 0";
            
            $result = SqlScripts::select($cols, "respostas", $condition);

            foreach($result as $value){

                if($value["correta"]){
                    $pontuacao ++;
                }
            }

            $score["created"] = date("Y-m-d H:i:s");
            $score["updated"] = date("Y-m-d H:i:s");
            $score["deleted"] = 0;
            $score["pontuacao"] = $pontuacao;

            $newRecord = SqlScripts::insert($score,"pontuacao");

            if($newRecord){
                echo "cadastro_com_sucesso";
            }
            else{
                echo "erro_sql";
            }

        break;

        case "pontuacao":

            $cols = ['pontuacao','duracao'];
            $condition =    "order by created desc
                                limit 5";
            
            $result = SqlScripts::select($cols, "pontuacao", $condition);

            $result = verificacao_quiz($result);

            echo json_encode($result);
        break;

        case "cadastrar":
            
            $data = $_POST["dados"];

            $quiz = [];
            $perguntas = [];
            $respostas = [];
            $perguntas_id = [];

            if( isset($data["nomeQuiz"]) ){

                $quiz["nome"] = $data["nomeQuiz"];
                $quiz["created"] = date("Y-m-d H:i:s");
                $quiz["updated"] = date("Y-m-d H:i:s");
                $quiz["deleted"] = false;

                $tabela_id = SqlScripts::insert($quiz, "quiz");

                unset($data["nomeQuiz"]);

                if(!empty($tabela_id)){

                    foreach($data as $key => $pergunta){

                        if(preg_match('/pergunta_\d*/', $key)){

                            $perguntas["titulo"]    = $pergunta;
                            $perguntas["quiz_id"]   = $tabela_id;
                            $perguntas["created"]   = date("Y-m-d H:i:s");
                            $perguntas["updated"]   = date("Y-m-d H:i:s");
                            $perguntas["deleted"]   = false;

                            $perguntas_id[ substr($key,9) ] = SqlScripts::insert($perguntas, "perguntas");

                        }

                    }

                    //verificando se a numeração da pergunta é igual a numeração da resposta para associar 
                    //o pergunta_id na resposta.
                    foreach($perguntas_id as $numero => $pergunta_id){

                        foreach($data as $key => $resposta){
                            
                            if( preg_match('/resp_\w*?_' . $numero . '_\d*/', $key) ){

                                $respostas["resposta"]      = $resposta;
                                $respostas["pergunta_id"]   = $pergunta_id;
                                $respostas["created"]       = date("Y-m-d H:i:s");
                                $respostas["updated"]       = date("Y-m-d H:i:s");

                                if(preg_match('/resp_correta_.*/', $key)){
                                    $respostas["correta"]   = true;
                                }
                                else{
                                    $respostas["correta"]   = false;
                                }

                                $teste = SqlScripts::insert($respostas,"respostas");

                            }
                        }
                    }

                    echo("quiz_cadastrado");
                }
            }
            else{
                echo("ausencia_de_dados");
            }
            
        break;
    }

//===========================   Sessão de Funções   ========================

/**
 * Realiza a validação das respostas do usuário.
 * 
 * @param data um array com todas as pontuações e duração do quiz em cache.
 * @return data um array associativo com as respostas aprovadas e reprovadas. 
 */
    function verificacao_quiz($data){
        
        foreach($data as $key => $value){
            if($value["pontuacao"] >= '6'){
                $data[$key]["avaliacao"] = "aprovado";
            }
            else{
                $data[$key]["avaliacao"] = "reprovado";
            }
        }

        return $data;
    }

?>