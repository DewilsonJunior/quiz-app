<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <title>Tela de Cadastro</title>
        <meta charset="utf-8"/>

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/cadastro_usuario.css"/>

    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">

                <div class="fadeIn">
                    <h2>Cadastre-se</h2>
                </div>

                <form id="form-cadastro" action="usuarioFunc.php" method="post">
                    <input type="text" id="login" class="fadeIn" name="login" placeholder="login"/>
                    <input type="text" id="email" class="fadeIn" name="email" placeholder="email"/>
                    <input type="password" id="senha" class="fadeIn" name="senha" placeholder="senha"/>
                    <input type="password" id="senhaRepeat" class="fadeIn" name="senhaRepeat" placeholder="Repita a senha"/>
                    <input type="submit" class="fadeIn" value="Cadastrar" id="btn-cadastrar"/>
                </form>

                <div id="formFooter" class="d-flex justify-content-between">

                </div>

            </div>
        </div>

        <script type="text/javascript" src="js/jquery.3.2.1.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/jquery.validate.js"></script>
        <script type="text/javascript" src="js/cadastro_usuario.js"></script>
    </body>
</html>