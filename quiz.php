<!DOCTYPE html>

<html lang="pt-br">
<head>
    <meta charset="utf-8" />

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css" />
    <link rel="stylesheet" type="text/css" href="css/quiz.css" />

    <?php 
        include_once("include/validaSessao.php");
    ?>

    <title>Quiz App</title>

</head>
<body>

    <div class="container mt-5">
        <div class="d-flex justify-content-center row">
            <div class="col-md-10" id="layout">

                <!-- Modal -->
                <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Quizz 1</h5>
                            </div>
                            <div class="modal-body d-flex justify-content-center">
                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-lg px-5 py-3" id="btn-modal">
                                    Começar!
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Fim Modal-->

                <!--Carrossel-->
                <div id="carousel_template" class="carousel slide" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner" id="carousel-body">

                    </div>
                </div>
                <!-- fim Carrossel-->

                <div class="d-flex flex-row justify-content-between align-items-center p-3 bg-white">
                    <button class="btn d-flex align-items-center btn-danger" type="button" id="btn-prev">
                        <i class="fa fa-angle-left mt-1 mr-1"></i>
                        &nbsp;Anterior
                    </button>
                    <button class="btn border-success align-items-center btn-success" type="button" id="btn-next">
                        Próximo
                        <i class="fa fa-angle-right ml-2"></i>
                    </button>
                </div>

                <div class="modal fade" id="pontuacao" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Pontuacao</h5>
                            </div>
                            <div class="modal-body d-flex justify-content-center">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Pontuação</th>
                                                <th scope="col">Duração</th>
                                                <th scope="col">Conclusão</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-center">
                                <button type="button" class="btn btn-primary" id="recomecar">Recomeçar</button>
                                <a href="index.php" class="btn btn-secondary">Sair</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <script type="text/javascript" src="js/jquery.3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="js/shuffle_plugin.js"></script>
    <script type="text/javascript" src="js/quiz.js"></script>
</body>
</html>
