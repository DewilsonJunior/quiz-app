<!DOCTYPE html>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css" />
    <link rel="stylesheet" type="text/css" href="css/cadastro_quiz.css" />

    <?php 
        include_once("include/validaSessao.php");
    ?>
</head>
<body>
    <div class="container my-5 pb-2 px-0 rounded shadow">
        <h1 class="d-flex justify-content-center text-light bg-dark rounded-top pb-2">Cadastro de Quiz</h1>
        <div class="row d-flex justify-content-end ">
            <div class="col-md-10">

                <form class="container" id="form-quiz">
                    <div class="form-group col-md-10">
                        <label for="nomeQuiz">Nome do Quiz</label>
                        <input type="text" class="form-control w-75" id="nomeQuiz" required="required"/>
                    </div>

                    <div class="fixed-bottom w-50 mt-3 ml-1" id="div-alert">
                        <!-- Aqui fica os alerts customizados -->
                    </div>

                    <!-- Aqui fica o quiz invocado pela função monta_formulario() -->

                </form>

            </div>
        </div>
    </div>

    <script type="text/javascript" src="js/jquery.3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
    <script src="http://jqueryvalidation.org/files/dist/jquery.validate.js"></script>
    <script type="text/javascript" src="js/cadastro_quiz.js"></script>
</body>
</html>