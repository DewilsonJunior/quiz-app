<?php

    include_once("conexao.php");

    class SqlScripts{
        
        //===============================================================
        //*
        //*Entrada: $colunas(array); $tabela(string); $condicao(string)
        //*Saida: return success(array); return error(string)
        //*
        //*===============================================================
        public function select($colunas = array(), $tabela = null, $condicao = null){

            $data = array();

            if(!empty($colunas) && !empty($tabela) && !empty($condicao)){

                $query =    "select ". implode(",", $colunas).
                                " from ". $tabela ." ".
                            $condicao;

                $con = $GLOBALS["con"];
                $result = $con->query($query);

                $data = $result->fetchAll(PDO::FETCH_ASSOC);

                return $data;
                
            }
            else{
                return "no_args";
            }
        }

        public function insert($data = array(), $tabela = null){

            $cols = [];

            if(!is_null($tabela)){
                $cols = array_keys($data);

                $query =    "insert into ". $tabela.
                                "(". implode(",", $cols) .")".
                            "values".
                                "('". implode("','", $data) ."')";

                $con = $GLOBALS["con"];
                $result = $con->exec($query);

                return $con->lastInsertId();
            }
            else{
                return "no_table";
            }
        }
    }

?>