<!DOCTYPE html>

<html lang="pt-br">
<head>
    <meta charset="utf-8" />

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css" />

    <?php 
        include_once("include/validaSessao.php");
    ?>

    <title>Quiz App</title>

</head>
<body>
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
                <div class="collapse navbar-collapse justify-content-md-center" >
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">
                                Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="cadastro_usuario.php">
                                Cadastrar Usuário
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="cadastro_quiz.php">
                                Cadastrar Quiz
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Cadastre e Jogue seu próprio Quiz.</h1>
        </div>
    </section>

    <div class="container" id="container">
    </div>
    
    <script type="text/javascript" src="js/jquery.3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
</body>
</html>
