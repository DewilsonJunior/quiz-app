<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <title>Tela de Login</title>
        <meta charset="utf-8"/>

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/login.css"/>
    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">

                <div class="fadeIn first">
                    <h2>Entrar</h2>
                </div>

                <form id="form-login">
                    <input type="text" id="login" class="fadeIn second" name="login" placeholder="login"/>
                    <input type="password" id="senha" class="fadeIn third" name="senha" placeholder="senha"/>
                    <input type="submit" class="fadeIn fourth" value="Entrar" />
                </form>

                <div id="formFooter" class="d-flex justify-content-between">
                    <a class="underlineHover" href="cadastro_usuario.php">Cadastre-se</a>
                    <a class="underlineHover" href="#">Esqueceu a senha?</a>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="js/jquery.3.2.1.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/jquery.validate.js"></script>
        <script type="text/javascript" src="js/login.js"></script>
    </body>
</html>