$("body").ready(function(){

    $.post("quizFunc.php",{action: "home"})
    .done(function(response){

        var data = JSON.parse(response);

        var num_quiz = Object.keys(data).length;

        if(num_quiz > 0){

            var rows = criar_linhas(num_quiz, data);
        }
        
    });
});


//===========================   Sessão de Funções   ========================

/**
 * Cria um card com base nos dados fornecidos como parametro.
 * 
 * @param {object} data um array com o id e nome do quiz.
 * @return "col" o card estruturado com suas devidas tags.
 */
function criar_cards(data){

    var col = $("<div></div>").addClass("col-md-4");
    var card = $("<div></div>").attr( {id: data["id"]} ).addClass("card mb-4 box-shadow");

    var cardHeader = $("<div></div>").addClass("card-header bg-secondary text-white");
    var nomeQuiz = $("<h2></h2>").addClass("text-center").html(data["nome"]);

    var cardBody = $("<div></div>").addClass("card-body");
    var dflex = $("<div></div>").addClass("d-flex justify-content-between align-items-center");
    var btnGroup = $("<div></div>").addClass("btn-group");
    var btn = $("<button>Jogar</button>").attr({type: "button"}).addClass("btn btn-outline-secondary");
    $(btn).click(chama_quiz);

    cardHeader.append(nomeQuiz);

    btnGroup.append(btn);
    dflex.append(btnGroup);
    cardBody.append(dflex);

    card.append([cardHeader, cardBody]);
    col.append(card);

    return col;

}

/**
 * 
 * @param {int} num_quiz o tamanho(length) do quiz.
 * @param {*} data um array de objetos contendo em cada objeto, o nome e id dos quizzes 
 * @return N/A
 */
function criar_linhas(num_quiz, data){

    console.log(data);

    //calculo ralizado para saber a quantidade de linhas em relação à colunas: 1 row para 3 cols.
    var num_linhas = parseInt( (num_quiz/3) ,10 ) + num_quiz % 3;
    var cols = [];

    //numero hard-code para a quantidade de colunas por linha.
    var quantidade_cols = 3;
    var j = 0;
    
    while(num_linhas > 0){

        var row = $("<div></div>").addClass("row");

        for(var i = 0; i < quantidade_cols; i++){

            if( typeof data[j] !== "undefined"){

                row.append( criar_cards(data[j]) );

                j++;
            }

        }

        //limpa o array cols para adicionar as novas colunas da proxima linha
        while( (deleted = cols.shift()) !== undefined){
        }

        $("#container").append(row);

        num_linhas--;
    }
    
}

/**
 * Função responsável por direcionar o usuário para o quiz selecionado pelo usuário.
 * 
 * @param N/A
 * @return N/A
 */
function chama_quiz(){

    quiz_id = $(this).parent().parent().parent().parent().attr("id");

    window.location.href = "quiz.php?quiz_id=" + quiz_id;

}