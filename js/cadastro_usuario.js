$("#form-cadastro").validate({
    rules: {
        login: "required",
        email: {
            required: true,
            email: true
        },
        senha: "required",
        senhaRepeat: {
            required: true,
            equalTo: "#senha"
        }
    },
    messages: {
        login: "Por favor, insira um login.",
        senha: "Por favor, insira uma senha.",
        email: {
            required: "Por favor, insira um email.",
            email: "O email não é valido, por favor digite novamente."
        },
        senhaRepeat: {
            required: "Por favor, repita a senha.",
            equalTo: "As senha encontra-se diferentes, por favor digite novamente."
        }
    },
    submitHandler: function(form){

        var dados = $(form).serialize();

        $.post("usuarioFunc.php", {action: "cadastrar", data: dados})
        .done(function(response){
            
            if(response == "usuario_cadastrado"){
                alert("Usuário cadastro com sucesso.");
                window.location.href = "login.php";
            }
            else if(response == "usuario_existente"){
                alert("Login usado por outro usuário, por favor escolha outro login.");
            }
            else if(response == "banco_error"){
                alert("ocorreu um erro ao tentar criar seu usuário, por favor tente novamente.");
            }
            else{
                alert("Ocorreu um erro no seu processo de cadastro. Por favor entre em contato com o suporte.");
            }
        });

        return false;
    }
});