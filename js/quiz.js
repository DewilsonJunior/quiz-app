var data = {};

$("body").ready(function(){

    var queryString = location.search.slice(1);
    var chaveValor = queryString.split("=");
    var quiz_id = chaveValor[1];

    $.post("quizFunc.php", {action: "carrega_quiz", quiz_id: quiz_id})
    .done(function(data){

        quiz = JSON.parse(data);
        var quiz_embaralhado = [];

        //$.shuffle é a chamada para um plugin que embaralha os dados do array gerando assim perguntas 
        //e respostas em ordem aleatória.  
        quiz_embaralhado = $.shuffle(quiz);

        $.each(quiz_embaralhado, function(index,value){

            criar_carrossel_itens(index, quiz.length);
            montar_pergunta(value,index);
        });

    })
    .always(function(){
        $('#modal').modal();
    });
    

});
    
$("#btn-prev").click(function(){

    $("#btn-next").removeClass("invisible");

    var layout = $(this).parent().parent();

    var paginacao = get_pagina_atual_tam_quiz(layout);
    
    if(paginacao['pergunta_atual'] == 1){
        $("#btn-prev").addClass("invisible");
    }
    else{
        $("#carousel_template").carousel('prev');
    }

});

$("#btn-next").click(function(){

    $("#btn-prev").removeClass("invisible");

    var layout = $(this).parent().parent();

    var paginacao = get_pagina_atual_tam_quiz(layout);

    if(paginacao['pergunta_atual'] == paginacao['quiz_length']){

        $("#btn-next").addClass("invisible");

        if(confirm('Clique em "OK" para confirmar suas respostas')){
            timer(false);
            consolida_dados(data);
        }
    }
    else{
        $("#carousel_template").carousel('next');
    }
});

$("#btn-modal").click(function(){
    timer(true);
});

$("#recomecar").click(function(){
    location.reload();
});

//===========================   Sessão de Funções   ========================

/**
* Monta uma pergunta e suas respectivas respostas para o quiz.
*
* @param {object} perg_e_resps um array com a pergunta e respostas.
* @param {int} carrossel_index um inteiro que correponde ao indice do carrossel
* @return N/A
*/
function montar_pergunta(perg_e_resps = null, carrossel_index = 0){

    var resp_embaralhadas = [];

    var pergunta = $("<h5></h5>").addClass('mt-1 ml-2');
    pergunta.html(perg_e_resps['pergunta']['titulo']);

    $("#pergunta-"+carrossel_index).append(pergunta);

    var respostas = perg_e_resps['pergunta']['resposta'];

    resp_embaralhadas = $.shuffle(respostas);

    $.each(resp_embaralhadas, function(index, value){ 

        var resposta = $("<div></div>").addClass("ans ml-2");
        var label = $("<label></label>").addClass("radio");
        var input = $("<input/>").attr( {type: "radio", name: "btn_radio", id: value['id']} );
        var span = $("<span></span>").html(value['resposta']);

        $(input).click((registra_resp_usuario));

        label.html([input, span]);
        resposta.html(label);

        $("#quiz-"+carrossel_index).append(resposta);

    });

}

/**
 * Cria um o layout de um item do carrossel. 
 * 
 * @param {int} num_quiz Corresponde a posição do item do carrossel Ex.: primeiro item, segundo item e etc. 
 * @param {*} quiz_length O tamanho do quiz, utilizado para numerar a paginação dos itens.
 * @return N/A
 */
function criar_carrossel_itens(num_quiz = 0, quiz_length){

    var paginacao = num_quiz + 1;

    if(num_quiz == 0){
        var carousel_item = $("<div></div>").addClass("carousel-item active");
    }
    else{
        var carousel_item = $("<div></div>").addClass("carousel-item");
    }
    
    var border = $("<div></div>").addClass("border").attr({id: "quiz-template-"+num_quiz});

    var question = $("<div></div>").addClass("question bg-white p-3 border-bottom");
    var d_flex1 = $("<div></div>").addClass("d-flex flex-row justify-content-between align-items-center mcq");
    d_flex1.append($(
        "<h4>MCQ Quiz</h4>"+
        "<span>("+ paginacao +" of "+ quiz_length +")</span>"
    ));

    var quiz = $("<div></div>").addClass("question bg-white p-3 border-bottom").attr({id: "quiz-"+num_quiz});
    var d_flex2 = $("<div></div>")
    .addClass("d-flex flex-row align-items-center question-title")
    .attr({name: "question", id: "pergunta-"+num_quiz});
    d_flex2.append($("<h3>Q.</h3>").addClass("text-Danger"));

    question.append(d_flex1);
    quiz.append(d_flex2);

    border.append(question, quiz);
    carousel_item.append(border);

    $("#carousel-body").append(carousel_item);
}

/**
 * Função que mostra atual item do carrossel e o tamanho do mesmo.
 * 
 * @param {object} layout: o layout atual, utilizado para verificar sua atual posição no corrossel.
 * @return {object} "data" um array com o tamanho do quiz + posição anterior do carrossel.
 */
function get_pagina_atual_tam_quiz(layout){

    var paginacao = layout.find(".active").find("div.d-flex > span").html();
    var data = [];

    paginacao = paginacao.split(" ");

    data['pergunta_atual'] = paginacao[0];
    data['pergunta_atual'] = data['pergunta_atual'].replace("(","");

    data['quiz_length'] = paginacao[paginacao.length-1];
    data['quiz_length'] = data['quiz_length'].replace(")","");

    return data;
}

/**
 * Gerencia as respostas selecionadas pelo usuário.
 * 
 * @param N/A
 * @return N/A
 */
function registra_resp_usuario(){

    var resp_usuario = $(this).attr("id");
    var quiz = $(this).parent().parent().parent();
    var pergunta = quiz.children().children();

    pergunta = pergunta.eq(1).html();

    if(!data.length){
        data[pergunta] = resp_usuario;
    }

    for(var key in data){
        if(key == pergunta){
            data[pergunta] = resp_usuario;
        }
        else{
            data[pergunta] = resp_usuario;
        }
    }
}

/**
 * Envia para o servidor todas as reposta selecionadas pelo usuário.
 * 
 * @param {object} data array com todas as perguntas e respostas.
 * @return N/A
 */
function consolida_dados(data){
    
    $.post("quizFunc.php",{action: "persistir", dados: data}, function(result){

        if(result == "erro_sql"){
            alert("ocorreu um erro ao salvar sua pontuação");
        }
        if(result == "cadastro_com_sucesso"){
            apresenta_pontuacao();
        }
        else{
            alert("Ocorreu um erro no quiz, por favor entre em contato com o suporte");
        }
    });
}

/**
 * Inicia ou para um timer que cronometra o tempo do jogo em partida.
 * 
 * @param {boolean} ativo paramentro responsável por ativar e desativar o cronometro. 
 * True = cronometro ativo e False = cronometro encerrado.
 * @return N/A
 */
function timer(ativo){
    
    if(ativo){
        var start = new Date;

        setInterval(function(){
            data["time"] = (new Date - start) / 1000;
        }, 1000);
    }
    else{
        clearInterval(data["time"]);
    }
}

/**
 * Apresenta para o usuário a conclusão do seu quiz jogado.
 * 
 * @param N/A
 * @return N/A
 */
function apresenta_pontuacao(){

    $.post("quizFunc.php", {action: "pontuacao"}, function(result){

        var data = JSON.parse(result);
        var tbody = $("#pontuacao").find("table").find("tbody");

        for(key in data){
            var tr = $("<tr></tr>");
            var th = $('<th scope="row"></th>');
            var td_pontuacao = $("<td></td>");
            var td_duracao = $("<td></td>");
            var td_conclusao = $("<td></td>");

            th.html(1 + parseInt(key));
            td_pontuacao.html(data[key]["pontuacao"]);
            td_duracao.html(data[key]["duracao"]);

            if(data[key]["avaliacao"] == "aprovado"){
                td_conclusao.html(data[key]["avaliacao"]);
                td_conclusao.addClass("aprovado");
            }
            else{
                td_conclusao.html(data[key]["avaliacao"]);
                td_conclusao.addClass("reprovado");
            }

            tr.append([th, td_pontuacao, td_duracao, td_conclusao]);

            tbody.append(tr);
        }

        $("#pontuacao").modal();
    });

}