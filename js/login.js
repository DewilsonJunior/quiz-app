$("#form-login").validate({
    rules: {
        login: "required",
        senha: "required"
    },
    messages: {
        login: "Este campo é obrigatório. Por favor insirir o login",
        senha: "Este campo é obrigatório. Por favor insirir a senha"
    },
    submitHandler: function(form){

        var dados = $(form).serialize();

        $.post("usuarioFunc.php", {action: "logar", data: dados})
        .done(function(response){
            if(response == "login_efetuado_com_sucesso"){
                window.location.href = "index.php";
            }
            else if(response == "usuario_incorreto"){
                alert("Login ou senha incorretos, por favor digite novamente.");
            }
            else{
                alert("Ocorreu um problema ao tentar logar com o seu usuário, tente novamente. " +
                       "Se persistir entre em contato com o suporte");
            }
        });
    }
});