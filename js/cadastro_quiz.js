$("body").ready(function(){

    //Variável responsável pelo tamanho do quiz
    var quiz_length = 10;

    monta_formulario(quiz_length);
});


//===========================   Sessão de Funções   ========================

/**
* Monta a estrutura de perguntas e respostas do formulário.
*
* @param quiz_length {int} o tamanho do quiz pré definido.
* @return N/A
*/
function monta_formulario(quiz_length){

    var i = 1;
    while(i <= quiz_length){

        var row = $("<div></div>").addClass("row justify-content-center w-75 shadow ml-3 mb-3 py-2");

        var div_pergunta = $("<div></div>").addClass("form-group col-md-10");
        var label_pergunta = $("<label></label>").attr({for: "pergunta-"+i }).html( "Pergunta "+i );
        var input_pergunta = $("<input></input>")
        .attr({type: "text", name: "pergunta_"+i, id: "pergunta_"+i, required: "required" })
        .addClass("form-control");
    
        var div_resposta_1 = $("<div></div>").addClass("form-group col-md-10 text-center");
        div_resposta_1.append($("<label></label>").html("Respostas da "+i));
        var row_resposta_1 = $("<div></div>").addClass("form-row");
    
        var div_resposta_2 = $("<div></div>").addClass("form-group col-md-10");
        var row_resposta_2 = $("<div></div>").addClass("form-row");
    
        var div_correta = $("<div></div>").addClass("col-md-6");
        var input_correta = $("<input></input>")
        .attr({type: "text", placeholder: "Correta", id: "resp_correta_"+i+"_1", required: "required"})
        .addClass("form-control");
    
        var div_errada_1 = $("<div></div>").addClass("col-md-6");
        var input_errada_1 = $("<input></input>")
        .attr({type: "text", placeholder: "Errada", id: "resp_errada_"+i+"_2", required: "required"})
        .addClass("form-control");
    
        var div_errada_2 = $("<div></div>").addClass("col-md-6");
        var input_errada_2 = $("<input></input>")
        .attr({type: "text", placeholder: "Errada", id: "resp_errada_"+i+"_3", required: "required"})
        .addClass("form-control");
    
        var div_errada_3 = $("<div></div>").addClass("col-md-6");
        var input_errada_3 = $("<input></input>")
        .attr({type: "text", placeholder: "Errada", id: "resp_errada_"+i+"_4", required: "required"})
        .addClass("form-control");
    
        div_pergunta.append( [label_pergunta, input_pergunta] );
    
        div_correta.append(input_correta);
        div_errada_1.append(input_errada_1);
        div_errada_2.append(input_errada_2);
        div_errada_3.append(input_errada_3);
    
        row_resposta_1.append( [div_correta, div_errada_1] );
        row_resposta_2.append( [div_errada_2, div_errada_3] );
    
        div_resposta_1.append(row_resposta_1);
        div_resposta_2.append(row_resposta_2);
    
        row.append( [div_pergunta, div_resposta_1, div_resposta_2] );
    
        $("#form-quiz").append(row);

        i++;
    }
    
    var btn_group = $("<div></div>").addClass("form-group col-md-10 d-flex justify-content-center");
    var btn_cadastrar = $("<button></button>").addClass("btn btn-success mx-1").html("Cadastrar");
    var btn_cancelar = $("<a></a>").attr({type: "button", href: "index.php"})
                        .addClass("btn btn-secondary mx-1").html("Cancelar");

    btn_group.append([btn_cadastrar,btn_cancelar]);

    $(btn_cadastrar).click(valida_formulario);

    $("#form-quiz").append(btn_group);
}

/**
* Valida os campos do formulário antes de enviar os dados.
*
* @param  N/A
* @return N/A
*/
function valida_formulario(event){

    event.preventDefault();

    var perguntasValue = {};
    var respostasValue = {};
    var data = {};
    var validado = true;

    $("input[type=text]").each(function(index){

        if($(this).val()){
            $(this).removeClass("is-invalid");
        }
        else{

            $(this).addClass("is-invalid");
            validado = false;
        }

        data[ $(this).attr("id") ] = $(this).val();

    });


    if(!validado){

        $("html").animate( {scrollTop:0}, 'slow' );

        alert_preenc_obrigatorio();
    }
    else{
        consolidar_dados(data);
    }


}

/**
* Alert personalizado utilizado no formulário.
*
* @param  N/A
* @return N/A
*/
function alert_preenc_obrigatorio(){

    var div = $("<div></div>").attr({role: "alert"}).addClass("alert alert-danger alert-dismissible fade show");
    div.append("<p>Todos os campos em<strong> vermelho</strong> são obrigatórios.</p>");

    var button = $("<button></button>").attr({'data-dismiss': "alert", 'arial-label': "Close"})
    .addClass("close");
    var span = $("<span>&times;</span>").attr({'aria-hidden': "true"});

    button.append(span);
    div.append(button);

    $("#div-alert").empty();
    $("#div-alert").append(div);

}

/**
 * Envia para o servidor todas as reposta selecionadas pelo usuário.
 * 
 * @param {object} data array com todas as perguntas e respostas.
 * @return N/A
 */
function consolidar_dados(data){

    $.post("quizFunc.php", {action: "cadastrar", dados: data})
    .done(function(response){

        if(response == "quiz_cadastrado"){

            alert("Quiz cadastrado com Sucesso");
            window.location.href = "index.php";
        }
        else if(response == "ausencia_de_dados"){
            alert("Não foi possível cadastrar o quiz por falta de informações, por favor revise o formulário.");
        }
        else{
            alert("Ocorreu um erro ao tentar cadastrar o quiz. Por favor entre em contato com o suporte.");
        }
    });
}

/**
 * Alerta customizado para casos de "success".
 * 
 * @param {string} mensagem mensagem a ser inserida no alert.
 * @return N/A
 */
function alert_customizado_success(mensagem){

    var div = $("<div></div>").attr({role: "alert"}).addClass("alert alert-success");
    div.html(mensagem);

    var button = $("<button></button>").attr({type: "button", 'data-dismiss': "alert", 'aria-label': "Close"})
    .addClass("close");
    var span = $("<span>&times;</span>").attr({'aria-hidden': true});

    button.append(span);
    div.append(button);

    $("#div-alert").empty();
    $("#div-alert").append(div);
}