CREATE TABLE `quiz_database`.`perguntas` ( 
    `id` INT NOT NULL AUTO_INCREMENT,
    `titulo` VARCHAR(255) NOT NULL,
    `quiz_id` INT(255) NOT NULL,
    `created` DATETIME NOT NULL, 
    `updated` DATETIME NOT NULL, 
    `deleted` BOOLEAN NOT NULL, 
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `quiz_database`.`respostas` (
    `id` INT NOT NULL AUTO_INCREMENT, 
    `resposta` VARCHAR(255) NOT NULL, 
    `pergunta_id` INT(255) NULL, 
    `created` DATETIME NOT NULL, 
    `updated` DATETIME NOT NULL, 
    `deleted` BOOLEAN NOT NULL, 
    `correta` BOOLEAN NOT NULL, 
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `quiz_database`.`pontuacao` ( 
    `id` INT NOT NULL AUTO_INCREMENT, 
    `pontuacao` INT(10) NOT NULL, 
    `duracao` VARCHAR(100) NULL, 
    `created` DATETIME NOT NULL, 
    `updated` DATETIME NOT NULL, 
    `deleted` BOOLEAN NOT NULL, 
    `usuario_id` INT(255) NULL, 
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `quiz_database`.`usuarios` ( 
    `id` INT NOT NULL AUTO_INCREMENT, 
    `login` VARCHAR(100) NOT NULL, 
    `senha` VARCHAR(100) NOT NULL, 
    `email` VARCHAR(100) NOT NULL,
    `created` DATETIME NOT NULL , 
    `updated` DATETIME NOT NULL, 
    `deleted` BOOLEAN NOT NULL, 
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `quiz_database`.`quiz` ( 
    `id` INT NOT NULL AUTO_INCREMENT, 
    `nome` VARCHAR(100) NOT NULL, 
    `created` DATETIME NOT NULL , 
    `updated` DATETIME NOT NULL, 
    `deleted` BOOLEAN NOT NULL, 
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;